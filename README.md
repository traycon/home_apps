# Home apps

Esta página pretende ser la principal desde la que se acceda al resto de servicios, solo se debe modificar el archivo services.json, contiene los servicios que se deben cargar.

** Para añadir un servicio en el json, debe añadir un nuevo objeto que contenga:
- desc => Descripción o nombre del servicio.
- url => Url donde se aloja el servicio.
- img => Debe contener la src de la imagen, puede ser una url, un base64, una referencia relativa, lo que sea que identifique la src de la imagen.

** Por defecto todas las imagenes se adaptan a 100px de alto, por si se quiere apurar en el peso y calidad de la imagen.

*** CUIDADO con las TILDES poner en UNICODE.